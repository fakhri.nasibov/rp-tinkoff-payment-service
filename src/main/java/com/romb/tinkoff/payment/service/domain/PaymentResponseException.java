package com.romb.tinkoff.payment.service.domain;

public class PaymentResponseException extends RuntimeException {
    public PaymentResponseException(String message) {
        super(message);
    }
}
