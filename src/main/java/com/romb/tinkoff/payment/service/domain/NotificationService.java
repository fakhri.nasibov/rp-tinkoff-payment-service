package com.romb.tinkoff.payment.service.domain;

public interface NotificationService<E> {
   void send(E event);
}