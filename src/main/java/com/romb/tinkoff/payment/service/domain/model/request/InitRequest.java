package com.romb.tinkoff.payment.service.domain.model.request;

import lombok.Data;

@Data
public class InitRequest {
    private String terminalKey;
    private Long amount;
    private String orderId;
}
