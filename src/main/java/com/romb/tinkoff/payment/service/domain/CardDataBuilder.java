package com.romb.tinkoff.payment.service.domain;

import com.romb.commons.payment.dto.CardDetails;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;

@Service
public class CardDataBuilder {

    public String build(CardDetails cardDetails) {
        String cardData = new StringBuilder()
                .append("PAN=").append(cardDetails.getNumber()).append(";")
                .append("ExpDate=").append(cardDetails.getExpMonthWithZero()).append(cardDetails.getExpYear()).append(";")
                .append("CardHolder=").append(cardDetails.getHolder()).append(";")
                .append("CVV=").append(cardDetails.getSecureCode())
                .toString();
        byte[] encryptedMessageBytes;
        try {
            // TODO remove Key generator when tinkoff will provide public key
            KeyPairGenerator generator  = KeyPairGenerator.getInstance("RSA");
            generator.initialize(2048);
            KeyPair pair = generator.generateKeyPair();
            PrivateKey privateKey = pair.getPrivate();
            PublicKey publicKey = pair.getPublic();
            Cipher encryptCipher = Cipher.getInstance("RSA");
            encryptCipher.init(Cipher.ENCRYPT_MODE, publicKey);
            byte[] secretMessageBytes = cardData.getBytes(StandardCharsets.UTF_8);
            encryptedMessageBytes = encryptCipher.doFinal(secretMessageBytes);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return Base64.getEncoder().encodeToString(encryptedMessageBytes);
    }
}
