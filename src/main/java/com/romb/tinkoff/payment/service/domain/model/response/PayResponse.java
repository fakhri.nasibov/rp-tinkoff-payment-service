package com.romb.tinkoff.payment.service.domain.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class PayResponse extends BaseResponse{
    @JsonProperty("Success")
    private Boolean success;
    @JsonProperty("ErrorCode")
    private String errorCode;
    @JsonProperty("TerminalKey")
    private String terminalKey;
    @JsonProperty("Status")
    private String status;
    @JsonProperty("PaymentId")
    private String paymentId;
    @JsonProperty("OrderId")
    private String orderId;
    @JsonProperty("Amount")
    private Integer amount;
}

