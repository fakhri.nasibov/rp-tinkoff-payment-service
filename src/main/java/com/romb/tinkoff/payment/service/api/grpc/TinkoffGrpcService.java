package com.romb.tinkoff.payment.service.api.grpc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.protobuf.BytesValue;
import com.google.protobuf.Empty;
import com.romb.commons.payment.dto.CardDetails;
import com.romb.commons.payment.mapper.CardDetailsMapper;
import com.romb.commons.utils.GrpcUtil;
import com.romb.tinkoff.payment.service.domain.PaymentDetails;
import com.romb.tinkoff.payment.service.domain.model.response.PayResponse;
import com.romb.tinkoff.payment.service.service.PaymentService;
import com.romb.tinkoff.payment.service.utils.PayToGrpcResponseMapper;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import org.lognet.springboot.grpc.GRpcService;
import payment.adapter.PaymentCommon;
import payment.adapter.TinkoffServiceGrpc;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.romb.commons.utils.MapperUtil.mapToBigDecimal;

@GRpcService
@RequiredArgsConstructor
public class TinkoffGrpcService extends TinkoffServiceGrpc.TinkoffServiceImplBase {

    private final PaymentService paymentService;
    private final CardDetailsMapper cardDetailsMapper;
    private final ObjectMapper objectMapper;


    @Override
    public void pay(PaymentCommon.PayRequest request, StreamObserver<PaymentCommon.PayResponse> responseObserver) {
        GrpcUtil.proceed(responseObserver, () -> {
            CardDetails cardDetails = cardDetailsMapper.map(request);
            PaymentDetails paymentDetails = new PaymentDetails(
                    request.getTerminal(),
                    mapToBigDecimal(request.getAmount()).setScale(2),
                    request.getPaymentId(),
                    request.getClientId(),
                    request.getSecretKey(),
                    request.getClientPhone()
            );
            PayResponse pay = paymentService.pay(cardDetails, paymentDetails);
            return PayToGrpcResponseMapper.map(pay);
        });
    }

    @Override
    public void getPaymentMethods(Empty request, StreamObserver<PaymentCommon.PaymentMethodsResponse> responseObserver) {

        GrpcUtil.proceed(responseObserver, () -> {
            List<PaymentCommon.PaymentMethod> allowedPaymentMethods = paymentService.getAllowedPaymentMethods()
                    .stream()
                    .map(paymentMethod -> PaymentCommon.PaymentMethod.newBuilder()
                            .setId(paymentMethod.getId())
                            .setType(paymentMethod.getType().name())
                            .setLabel(paymentMethod.getLabel())
                            .build())
                    .toList();
            return PaymentCommon.PaymentMethodsResponse.newBuilder().addAllMethods(allowedPaymentMethods).build();
        });
    }

    @Override
    public void finish3dsPayment(BytesValue request, StreamObserver<Empty> responseObserver) {
        GrpcUtil.proceed(responseObserver, () -> {
            byte[] bytes = request.getValue().toByteArray();
            Map data;
            try {
                data = objectMapper.readValue(bytes, HashMap.class);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            paymentService.complete3Ds((String) data.get("PaRes"), (String) data.get("MD"));
            return Empty.getDefaultInstance();
        });
    }
}
