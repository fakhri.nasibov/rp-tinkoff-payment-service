package com.romb.tinkoff.payment.service.domain.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class InitResponse extends BaseResponse{
    @JsonProperty("TerminalKey")
    private String terminalKey;
    @JsonProperty("Amount")
    private Integer amount;
    @JsonProperty("OrderId")
    private String orderId;
    @JsonProperty("Success")
    private Boolean success;
    @JsonProperty("Status")
    private String status;
    @JsonProperty("PaymentId")
    private String paymentId;
    @JsonProperty("ErrorCode")
    private String errorCode;
}
