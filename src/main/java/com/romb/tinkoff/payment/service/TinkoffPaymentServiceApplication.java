package com.romb.tinkoff.payment.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.romb"})
@ConfigurationPropertiesScan("com.romb.commons.kafka.config.properties")
public class TinkoffPaymentServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(TinkoffPaymentServiceApplication.class, args);
    }

}
