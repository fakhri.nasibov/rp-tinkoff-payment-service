package com.romb.tinkoff.payment.service.domain.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Check3DSVersionResponse extends BaseResponse {
    @JsonProperty("Success")
    private Boolean success;
    @JsonProperty("ErrorCode")
    private String errorCode;
    @JsonProperty("Message")
    private String message;
    @JsonProperty("Version")
    private String version;
    @JsonProperty("TdsServerTransID")
    private String tdsServerTransID;
    @JsonProperty("ThreeDSMethodURL")
    private String threeDSMethodURL;
    @JsonProperty("PaymentSystem")
    private String paymentSystem;
}
