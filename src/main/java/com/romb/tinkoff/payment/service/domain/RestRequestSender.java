package com.romb.tinkoff.payment.service.domain;

import com.romb.commons.kafka.dto.LoggingEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Slf4j
@Service
public class RestRequestSender {

    private final NotificationService<LoggingEvent> loggingNotificationService;
    private final RestTemplate restTemplate;
    public RestRequestSender(NotificationService<LoggingEvent> loggingNotificationService) {
        this.loggingNotificationService = loggingNotificationService;
        this.restTemplate = new RestTemplate();
    }


    public  <R> R send(HttpEntity<?> httpEntity, String url, Class<R> responseType, String author, HttpMethod method) {
        try {
            ResponseEntity<R> responseEntity = restTemplate.exchange(
                    url,
                    method,
                    httpEntity,
                    responseType
            );

            if (responseEntity.getBody() == null) {
                throw new PaymentResponseException("Can not fetch payment result!");
            }
            var resp = responseEntity.getBody();
            if (resp == null) {
                throw new PaymentResponseException("Payment failed. Response is null.");
            }
            LoggingEvent event = LoggingEvent.from(resp, Collections.singletonList(httpEntity), author);
            loggingNotificationService.send(event);
            return resp;
        } catch (RestClientException e) {
            log.error("Payment failed. Error: " + e.getMessage());
            throw new PaymentResponseException("Payment failed. Internal Error.");
        }
    }
}
