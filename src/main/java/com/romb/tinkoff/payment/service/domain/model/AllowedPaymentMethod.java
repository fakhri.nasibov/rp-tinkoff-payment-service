package com.romb.tinkoff.payment.service.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AllowedPaymentMethod {

    private String id;
    private AllowedPaymentMethodType type;
    private String label;

}