package com.romb.tinkoff.payment.service.domain.infrastructure;

import com.romb.commons.kafka.dto.LoggingEvent;
import com.romb.commons.kafka.service.KafkaService;
import com.romb.tinkoff.payment.service.domain.NotificationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class KafkaLoggingEventNotificationService implements NotificationService<LoggingEvent> {

    @Value("${spring.kafka.journal.topic}")
    private String topicName;

    private final KafkaService kafkaService;

    @Override
    public void send(LoggingEvent event) {
        kafkaService.send(event);
        log.info("Send message to topic \"" + topicName + "\" with id: " + event.getId());
    }
}
