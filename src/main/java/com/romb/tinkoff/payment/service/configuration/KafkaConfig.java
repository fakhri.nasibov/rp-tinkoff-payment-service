package com.romb.tinkoff.payment.service.configuration;

import com.romb.commons.kafka.config.KafkaConfiguration;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;

@EnableKafka
@Configuration
@RequiredArgsConstructor
public class KafkaConfig extends KafkaConfiguration {
}
