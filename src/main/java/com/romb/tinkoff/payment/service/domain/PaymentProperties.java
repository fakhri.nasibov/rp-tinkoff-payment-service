package com.romb.tinkoff.payment.service.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties(prefix = "payment")
public class PaymentProperties {
    private String baseUrl;
    private String initUri;
}
