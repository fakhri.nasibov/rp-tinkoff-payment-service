package com.romb.tinkoff.payment.service.service;

import com.romb.commons.payment.dto.CardDetails;
import com.romb.tinkoff.payment.service.domain.PaymentDetails;
import com.romb.tinkoff.payment.service.domain.PaymentOperation;
import com.romb.tinkoff.payment.service.domain.model.AllowedPaymentMethod;
import com.romb.tinkoff.payment.service.domain.model.response.PayResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PaymentService {

    private final PaymentOperation paymentOperation;


    public PayResponse pay(CardDetails cardDetails, PaymentDetails paymentDetails) {
       return paymentOperation.pay(cardDetails, paymentDetails);
    }

    public List<AllowedPaymentMethod> getAllowedPaymentMethods() {
        return paymentOperation.getAllowedPaymentMethods();
    }

    public void complete3Ds(String paRes, String md) {
        paymentOperation.complete3Ds(paRes, md);
    }
}
