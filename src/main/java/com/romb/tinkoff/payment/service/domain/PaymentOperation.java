package com.romb.tinkoff.payment.service.domain;

import com.romb.commons.payment.dto.CardDetails;
import com.romb.tinkoff.payment.service.domain.model.AllowedPaymentMethod;
import com.romb.tinkoff.payment.service.domain.model.AllowedPaymentMethodType;
import com.romb.tinkoff.payment.service.domain.model.request.Check3DSVersionRequest;
import com.romb.tinkoff.payment.service.domain.model.request.InitRequest;
import com.romb.tinkoff.payment.service.domain.model.request.PayRequest;
import com.romb.tinkoff.payment.service.domain.model.response.Check3DSVersionResponse;
import com.romb.tinkoff.payment.service.domain.model.response.InitResponse;
import com.romb.tinkoff.payment.service.domain.model.response.PayResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@EnableConfigurationProperties({PaymentProperties.class})
public class PaymentOperation {

    private final PaymentProperties paymentProperties;
    private final RestRequestSender restRequestSender;
    private final CardDataBuilder cardDataBuilder;

    public PayResponse pay(CardDetails cardDetails, PaymentDetails paymentDetails) {
        InitResponse init = init(paymentDetails);
        Check3DSVersionResponse threeDSVersion = check3DSVersion(cardDetails, paymentDetails, init);
        // todo add logic based on 3ds version
        return authorize(cardDetails, paymentDetails, init);
    }

    public InitResponse init(PaymentDetails paymentDetails) {
        InitRequest initRequest = new InitRequest();
        initRequest.setAmount(paymentDetails.getAmount().longValue());
        initRequest.setTerminalKey(paymentDetails.getTerminal());
        initRequest.setOrderId(paymentDetails.getPaymentId());
        return restRequestSender.send(
                new HttpEntity<>(initRequest),
                paymentProperties.getBaseUrl() + paymentProperties.getInitUri(),
                InitResponse.class,
                paymentDetails.getClientId(),
                HttpMethod.POST
        );
    }

    public Check3DSVersionResponse check3DSVersion(CardDetails cardDetails, PaymentDetails paymentDetails, InitResponse initResponse) {
        Check3DSVersionRequest request = new Check3DSVersionRequest();
        request.setPaymentId(initResponse.getPaymentId());
        request.setCardData(cardDataBuilder.build(cardDetails));
        request.setTerminalKey(paymentDetails.getTerminal());
        request.setToken(paymentDetails.getSecretKey());
        return restRequestSender.send(
                new HttpEntity<>(request),
                paymentProperties.getBaseUrl() + paymentProperties.getInitUri(),
                Check3DSVersionResponse.class,
                paymentDetails.getClientId(),
                HttpMethod.POST
        );
    }
    public PayResponse authorize(CardDetails cardDetails, PaymentDetails paymentDetails, InitResponse initResponse) {
        PayRequest payRequest = new PayRequest();
        payRequest.setPaymentId(initResponse.getPaymentId());
        payRequest.setCardData(cardDataBuilder.build(cardDetails));
        payRequest.setTerminalKey(paymentDetails.getTerminal());
        payRequest.setToken(paymentDetails.getSecretKey());
        return restRequestSender.send(
                new HttpEntity<>(payRequest),
                paymentProperties.getBaseUrl() + paymentProperties.getInitUri(),
                PayResponse.class,
                paymentDetails.getClientId(),
                HttpMethod.POST
        );
    }

    public List<AllowedPaymentMethod> getAllowedPaymentMethods() {
        ArrayList<AllowedPaymentMethod> paymentMethods = new ArrayList<>();
        paymentMethods.add(new AllowedPaymentMethod("v56Ef", AllowedPaymentMethodType.CARD, "Оплата картой"));

        return paymentMethods;
    }

    public void complete3Ds(String paRes, String md) {
        // TODO Finish3Ds logic
    }
}
