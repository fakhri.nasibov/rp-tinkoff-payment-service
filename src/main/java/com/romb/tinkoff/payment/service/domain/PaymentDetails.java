package com.romb.tinkoff.payment.service.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PaymentDetails {
    private String terminal;
    private BigDecimal amount;
    private String paymentId;
    private String clientId;
    private String secretKey;
    private String clientPhone;
}